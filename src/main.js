import { createApp } from 'vue'
import { createPinia } from 'pinia'
import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';
import VueApexCharts from "vue3-apexcharts";
import ApexCharts from 'vue3-apexcharts';
import App from './App.vue'
import router from './router'
import axios from 'axios'
import VueCookies from "vue-cookies";
import './assets/main.css'
import { inject} from 'vue';

const app = createApp(App)

app.use(createPinia());
app.use(router);
app.use(Antd);
app.use(VueApexCharts);
app.use(VueCookies);

app.mount('#app')

//app.config.globalProperties.$http = axios;
app.config.globalProperties.$apexcharts = ApexCharts;

//Inject the $cookies here so you can use it globally in your components
const $cookies = inject('$cookies'); 

axios.defaults.baseURL = import.meta.env.VITE_BASE_URL