import { createRouter, createWebHistory } from 'vue-router'
import DashboardView from '../views/DashboardView.vue'
import { cookieData } from '@/constants';
import { useAuthStore } from "@/stores/authStore";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/sign-up',
      name: 'signUp',
      // route level code-splitting
      // this generates a separate chunk (SignUp.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/SignUpView.vue' /* webpackChunkName: "SignUpView" */),      
    },
    {
      path: '/activate-account',
      name: 'activateAccount',
      // route level code-splitting
      // this generates a separate chunk (SignUp.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/ActivateAccountView.vue' /* webpackChunkName: "ActivateAccountView" */),      
    },
    {
      path: '/',
      name: 'defaultPage',     
      //defaultPage is automatically redirected to Dashboard Page
      redirect: '/dashboard',
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('../views/LoginMainView.vue' /* webpackChunkName: "LoginMainView" */)
    },
    {
      path: '/forgot-password',
      name: 'forgotPassword',
      component: () => import('../views/ForgotPasswordView.vue' /* webpackChunkName: "ForgotPasswordView" */)
    },
    {
      path: '/reset-password',
      name: 'resetPassword', 
      component: () => import('../views/ResetPasswordView.vue' /* webpackChunkName: "ResetPasswordView" */)
    },
    {
      path: '/profile',
      name: 'profile',
      component: () => import('../views/ProfileView.vue' /* webpackChunkName: "ProfileView" */),
      meta : {
        requiresAuth:true
      }
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: DashboardView,
      meta : {
        requiresAuth:true
      }
    },
    {
      path: '/products',
      name: 'products',
      component: () => import('../views/ProductView.vue' /* webpackChunkName: "ProductView" */),
      meta : {
        requiresAuth:true
      }
    },
    {
      path: '/categories',
      name: 'categories',
      component: () => import('../views/CategoryView.vue' /* webpackChunkName: "CategoryView" */),
      meta : {
        requiresAuth:true
      }
    },
    {
      path: '/suppliers',
      name: 'suppliers',
      component: () => import('../views/SupplierView.vue' /* webpackChunkName: "SupplierView" */),
      meta : {
        requiresAuth:true
      }
    },
    {
      path: '/manufacturers',
      name: 'manufacturers',
      component: () => import('../views/ManufacturerView.vue' /* webpackChunkName: "ManufacturerView" */),
      meta : {
        requiresAuth:true
      }
    },
    {
      path: '/brands',
      name: 'brands',
      component: () => import('../views/BrandView.vue' /* webpackChunkName: "BrandView" */),
      meta : {
        requiresAuth:true
      }
    },
    {
      path: '/stores',
      name: 'stores',
      component: () => import('../views/StoreView.vue' /* webpackChunkName: "StoreView" */),
      meta : {
        requiresAuth:true
      }
    },
    {
      path: '/customers',
      name: 'customers',
      component: () => import('../views/CustomerView.vue' /* webpackChunkName: "CustomerView" */),
      meta : {
        requiresAuth:true
      }
    },
    {
      path: '/purchase-orders',
      name: 'purchaseOrders',
      component: () => import('../views/PurchaseOrderView.vue' /* webpackChunkName: "PurchaseOrderView" */),
      meta : {
        requiresAuth:true
      }
    },
    {
      path: '/sale-transactions',
      name: 'saleTransactions',
      component: () => import('../views/SaleTransactionView.vue' /* webpackChunkName: "SaleTransactionView" */),
      meta : {
        requiresAuth:true
      }
    },
    {
      path: '/employees',
      name: 'employees',
      component: () => import('../views/EmployeeView.vue' /* webpackChunkName: "EmployeeView" */),
      meta : {
        requiresAuth:true
      }
    },
    {
      path: '/reports/report1',
      name: 'report1',
      component: () => import('../views/Report1View.vue' /* webpackChunkName: "Report1View" */),
      meta : {
        requiresAuth:true
      }
    },
    {
      path: '/reports/report2',
      name: 'report2',
      component: () => import('../views/Report2View.vue' /* webpackChunkName: "Report2View" */),
      meta : {
        requiresAuth:true
      }
    },
    {
      path: '/reports/report3',
      name: 'report3',
      component: () => import('../views/Report3View.vue' /* webpackChunkName: "Report3View" */),
      meta : {
        requiresAuth:true
      }
    },
    {
      path: '/settings/users',
      name: 'users',
      component: () => import('../views/UserView.vue' /* webpackChunkName: "UserView" */),
      meta : {
        requiresAuth:true
      }
    },
    {
      path: '/settings/roles',
      name: 'roles',
      component: () => import('../views/RoleView.vue' /* webpackChunkName: "RoleView" */),
      meta : {
        requiresAuth:true
      }
    },
    {
      path: '/logout',
      name: 'logout',
      beforeEnter: (to, from) => {

        const { logOut } = useAuthStore();       
        // const { resetAllStores } = useResetAllStores()

        const answer = window.confirm('Are you sure you want to log out?')

        if (!answer) {
          return false}
        else{ 
          console.log('Logging out')
          logOut()
          //then redirect to login page
          router.push('/login')
          //resetAllStores()

         }
      },       
    },
    {
      //if user browse a url that does not exist 
      //then redirect to base url '/'
      path: '/:catchAll(.*)',
      redirect:'/'
    },
  ]
})

router.beforeEach((to, from, next) => {

  const requiresAuth = to.matched.some(record => record.meta.requiresAuth)
  const isAuthenticated = $cookies.get(cookieData.accessTokenName)  
  const { checkAccessStillValid } = useAuthStore();

  if (!requiresAuth) {
    next() //just allow user to browse the requested URL
  } else if(requiresAuth && !isAuthenticated){  
    next('/login') //redirect user to login page
  }
  else {  
    checkAccessStillValid(); //check if user access still valid
    next() //allow to browse the requested URL
  }
 
})

export default router
