import { ref, reactive } from 'vue';
import { defineStore } from 'pinia';
import axios from "axios";
import { useNotification } from '../composables/useNotification';
import { cookieData } from '@/constants';
import { useRouter } from 'vue-router';



export const useAccountStore = defineStore('accountStore', () => {

 //data *************************************/  
  const accountDataSource = ref([]); 
  const accountDataSourceMeta = ref([]);
  const accountOptionListSource = ref([]);
  const landing_page_url = ref('/login')

  const router = useRouter();
  const viewAccountFormRequest=reactive({
    page: 1,
    per_page: parseInt(import.meta.env.VITE_DEFAULT_PAGINATION_SIZE),
    filter_allcolumn: null,
    filter_activatewildcard: null,
    filter_id: null,
    filter_name: null,
    filter_email: null,
    filter_website: null,
    filter_phone1: null,
    filter_phone2: null,
    filter_phone3: null,
    filter_fax: null,
    filter_address1: null,
    filter_address2: null,
    filter_zip_postal_code: null,
    filter_city: null,
    filter_subnational_entity: null,
    filter_country_id: null,    
    filter_note: null,
    filter_status: null,
    filter_tag: null,
    filter_created_at: null,

    sort_id: 0,
    sort_name: 0,
    sort_email: 0,
    sort_website: 0,
    sort_phone1: 0,
    sort_phone2: 0,
    sort_phone3: 0,
    sort_fax: 0,
    sort_address1: 0,
    sort_address2: 0,
    sort_zip_postal_code: 0,
    sort_city: 0,
    sort_subnational_entity: 0,
    sort_country_id: 0,  
    sort_note: 0,
    sort_status: 0,
    sort_tag: 0,
    sort_created_at: 0,

    export_to: '',
  })

  const registerAccountFormRequest=reactive({
    first_name: null,
    last_name: null,
    email: null,   
    address1: null, 
    city: null,
    zip_postal_code: null,
    subnational_entity: null,
    country_id: null,  
    password: '',
    confirm_password: '',
  })

  const registerAccountFormRequestLoading = ref(false)

  const activateAccountFormRequest=reactive({
    email: null,  
    activation_code: '',
  })

  

  const updateAccountFormRequest=reactive({
    page: 1,    
    sort_created_at: null,
    export_to: '',
  })

  const deleteAccountFormRequest=reactive({
    page: 1,    
    sort_created_at: null,
    export_to: '',
  })

  const restoreAccountFormRequest=reactive({
    page: 1,    
    sort_created_at: null,
    export_to: '',
  })

  const accountLoading = ref(false)

  //const landing_page_url = ref('/dashboard')
  const { openNotificationWithIcon } = useNotification()


  //getters *********************************/
  //const doubleCount = computed(() => count.value++ )

  //methods ********************************/
  function listAccount(){
    accountLoading.value = true; 
    accountDataSource.value = []   
      axios
        .post("/account",viewAccountFormRequest, {
          headers: { Authorization: `Bearer ${$cookies.get(cookieData.accessTokenName)}` },
        })     
        .then((res) => { 
          //save the retrieved data to accountDataSource state       
          accountDataSource.value = res.data.data 
          accountDataSourceMeta.value = res.data.meta
          accountDataSourceMeta.value['from'] = accountDataSourceMeta.value['from'].toLocaleString()
          accountDataSourceMeta.value['to'] = accountDataSourceMeta.value['to'].toLocaleString()
          accountDataSourceMeta.value['total'] = accountDataSourceMeta.value['total'].toLocaleString()
          accountLoading.value = false;                    
          //openNotificationWithIcon('success',res.data.message);      
          
          })
        .catch((err) => {
          accountLoading.value = false;  
          openNotificationWithIcon('error',err.response.data.message);           
        });   
  }

  function registerAccount(){     
    registerAccountFormRequestLoading.value=true
      axios
        .post("/account/register",registerAccountFormRequest, {
          headers: { Authorization: `Bearer ${$cookies.get(cookieData.accessTokenName)}` },
        })     
        .then((res) => {    
          registerAccountFormRequestLoading.value=false    
          openNotificationWithIcon('success',res.data.message,10); 
          router.push('/activate-account')    
          })
        .catch((err) => {    
          registerAccountFormRequestLoading.value=false      
          openNotificationWithIcon('error',err.response.data.message);           
        });   
  }

  function activateAccount(){     
    axios
      .post("/account/activate",activateAccountFormRequest, {
        headers: { Authorization: `Bearer ${$cookies.get(cookieData.accessTokenName)}` },
      })     
      .then((res) => {        
        openNotificationWithIcon('success',res.data.message,5);      
        router.push(landing_page_url.value)
        })
      .catch((err) => {          
        openNotificationWithIcon('error',err.response.data.message,5);           
      });   
  }


  function updateAccount(){    
  }

  function deleteAccount(id){   
    accountLoading.value = true; 
    //accountDataSource.value = []   
      axios
        .get(`/account/delete/${id}`,{
          headers: { Authorization: `Bearer ${$cookies.get(cookieData.accessTokenName)}` },
        })     
        .then((res) => { 
          //save the retrieved data to accountDataSource state       
          //accountDataSource.value = res.data.data 
          //accountDataSourceMeta.value = res.data.meta
          accountLoading.value = false;                    
          openNotificationWithIcon('success',res.data.message);      
          
          })
        .catch((err) => {
          accountLoading.value = false;  
          openNotificationWithIcon('error',err.response.data.message);           
        });    
  }

  function restoreAccount(){    
  }

  function optionListAccount(){  
 
    axios
      .get(`/account/option-list`,{
        headers: { Authorization: `Bearer ${$cookies.get(cookieData.accessTokenName)}` },
      })     
      .then((res) => { 
        //save the retrieved data to categoryDataSource state       
        accountOptionListSource.value = res.data.data                    
        //openNotificationWithIcon('success',res.data.message); 
        //console.log('burt',accountOptionListSource.value)     
        })
      .catch((err) => {  
        //openNotificationWithIcon('error',err.response.data.message);           
      });    
}


  return { 
    accountLoading,
    accountDataSource, 
    registerAccountFormRequest,
    activateAccountFormRequest,
    accountDataSourceMeta,
    viewAccountFormRequest,
    updateAccountFormRequest,
    deleteAccountFormRequest,
    restoreAccountFormRequest,
    accountOptionListSource,
    registerAccountFormRequestLoading,
    listAccount, 
    registerAccount, 
    activateAccount,
    updateAccount, 
    deleteAccount,  
    restoreAccount,
    optionListAccount
 
  }
})
