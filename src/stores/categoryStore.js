import { ref, reactive, toRaw } from 'vue'
import { defineStore } from 'pinia'
import axios from 'axios'
import { useNotification } from '../composables/useNotification'
import { cookieData, systemMessages } from '@/constants'


export const useCategoryStore = defineStore('categoryStore', () => {
  //data *************************************/
  const categoryDataSource = ref([])
  const categoryDataSourceMeta = ref([])
  const categoryOptionListSource = ref([])
  const categoryDataSourceMetaTotalCache = reactive({})
  const categoryDataSourceMetaLastPage = reactive({})
  const categoryExportLoading = ref(false)
  const categoryPageButtonEnabled = ref(false)
  const  currentSortedColumn = ref(null);

  let controller = null
  let viewCategoryFormRequestCache = reactive({})

  const systemMessage = systemMessages;


  const viewCategoryFormRequest = reactive({
    page: 1,
    per_page: import.meta.env.VITE_DEFAULT_PAGINATION_SIZE,
    filter_allcolumn: null,
    filter_activatewildcard: null,
    filter_id: null,
    filter_name: null,
    filter_description: null,
    filter_status: null,
    filter_created_at: null,

    sort_id: 0,
    sort_name: 0,
    sort_description: 0,
    sort_status: 0,
    sort_created_at: 0,

    export_to: '',
    count_only: 0
  })

  const storeCategoryFormRequest = reactive({
    name: null,
    description: null,
    status: null
  })
  const updateCategoryFormRequest = reactive({
    name: null,
    description: null,
    status: null
  })

  const deleteCategoryFormRequest = reactive({
    name: null,
    description: null,
    status: null
  })

  const restoreCategoryFormRequest = reactive({
    name: null,
    description: null,
    status: null
  })

  const categoryLoading = ref(false)
  const viewCategoryFormRequestLoading = ref(false)
  const storeCategoryFormRequestLoading = ref(false)
  const updateCategoryFormRequestLoading = ref(false)
  const deleteCategoryFormRequestLoading = ref(false)

  //const landing_page_url = ref('/dashboard')
  const { openNotificationWithIcon } = useNotification()

  //getters *********************************/
  //const doubleCount = computed(() => count.value++ )

  //methods ********************************/
  function objectsHaveSameContentExcept(obj1, obj2, excludedAttribute) {
    const keys1 = Object.keys(obj1)
    const keys2 = Object.keys(obj2)

    if (keys1.length !== keys2.length) {
      return false
    }

    for (const key of keys1) {
      if (key !== excludedAttribute && !key.startsWith('sort_') && obj1[key] !== obj2[key]) {
        return false
      }
    }

    return true
  }

  function exportToFile(event) {
    //console.log('burtevent',event.key)
    categoryExportLoading.value = true

    //set what format to use on export
    viewCategoryFormRequest.export_to = event.key

    //set the count_only to 0 to indicate that this request
    //is NOT counting but to really get the data results
    viewCategoryFormRequest.count_only = 0

    axios
      .post('/category', viewCategoryFormRequest, {
        responseType: 'blob',
        headers: { Authorization: `Bearer ${$cookies.get(cookieData.accessTokenName)}` }
      })
      .then((res) => {
        openNotificationWithIcon('success', systemMessage.exportSuccess)
        const url = URL.createObjectURL(new Blob([res.data]))
        const link = document.createElement('a')
        link.href = url
        link.setAttribute('download', 'category.' + viewCategoryFormRequest.export_to)
        document.body.appendChild(link)
        link.click()
        document.body.removeChild(link)
        viewCategoryFormRequest.export_to = ''
        categoryExportLoading.value = false
      })
      .catch((err) => {
        viewCategoryFormRequest.export_to = ''
        categoryExportLoading.value = false
        openNotificationWithIcon('error', err.response.data.message)
      })
  }

  function listCategory() {
    viewCategoryFormRequestLoading.value = true
    categoryDataSource.value = []
    categoryDataSourceMeta.value['from'] = '--'
    categoryDataSourceMeta.value['to'] = '--'
    categoryDataSourceMeta.value['total'] = '--'

    //set the count_only to 0 to indicate that this request
    //is NOT counting but to really get the data results
    viewCategoryFormRequest.count_only = 0
    axios
      .post('/category', viewCategoryFormRequest, {
        headers: { Authorization: `Bearer ${$cookies.get(cookieData.accessTokenName)}` }
      })
      .then((res) => {
        //save the retrieved data to categoryDataSource state
        categoryDataSource.value = res.data.data
        categoryDataSourceMeta.value = res.data.meta
        //categoryDataSourceMeta.value['from'] = categoryDataSourceMeta.value['from'].toLocaleString()
        //categoryDataSourceMeta.value['to'] = categoryDataSourceMeta.value['to'].toLocaleString()
        //categoryDataSourceMeta.value['total'] = categoryDataSourceMeta.value['total'].toLocaleString()
        viewCategoryFormRequestLoading.value = false
        //openNotificationWithIcon('success',res.data.message);
        listCategoryResultCount()
      })
      .catch((err) => {
        viewCategoryFormRequestLoading.value = false
        categoryDataSourceMeta.value['from'] = '0'
        categoryDataSourceMeta.value['to'] = '0'
        categoryDataSourceMeta.value['total'] = '0'
        openNotificationWithIcon('error', err.response.data.message)
      })
  }

  function listCategoryResultCount() {
    //console.log('burt cache',viewCategoryFormRequestCache.page)
    //console.log('burt request',viewCategoryFormRequest.page)
    const haveSameContent = objectsHaveSameContentExcept(
      toRaw(viewCategoryFormRequest),
      viewCategoryFormRequestCache,
      'page'
    )
    //console.log('burt same content',haveSameContent)

    if (haveSameContent) {
      //openNotificationWithIcon('error', 'Same query request is submitted cancelling execution')
      categoryDataSourceMeta.value['total'] = categoryDataSourceMetaTotalCache.value
      categoryDataSourceMeta.value['last_page'] = categoryDataSourceMetaLastPage.value
      return
    } else {
      categoryDataSourceMeta.value['total'] = '(counting..)'
      //openNotificationWithIcon('success', 'New query request detected proceed to execution')
      viewCategoryFormRequestCache = { ...toRaw(viewCategoryFormRequest) }
      categoryDataSourceMetaLastPage.value = null
      categoryDataSourceMetaTotalCache.value = null
    }

    categoryPageButtonEnabled.value = false

    if (controller != null) {
      controller.abort()
      //openNotificationWithIcon('error', 'Current counting aborted, Executing new count..')
      controller = null
    }

    //console.log('Burt',controller)
    controller = new AbortController()

    //set the count_only to 1 to indicate that this request
    //is for counting results only
    viewCategoryFormRequest.count_only = 1

    axios
      .post('/category', viewCategoryFormRequest, {
        signal: controller.signal,
        headers: { Authorization: `Bearer ${$cookies.get(cookieData.accessTokenName)}` }
      })
      .then((res) => {
        //save the retrieved data to categoryDataSourceMeta state
        categoryDataSourceMeta.value['last_page'] = res.data.last_page
        categoryDataSourceMetaLastPage.value = res.data.last_page
        categoryDataSourceMeta.value['total'] = res.data.result_count
        categoryDataSourceMetaTotalCache.value = categoryDataSourceMeta.value['total']
        categoryPageButtonEnabled.value = true
      })
      .catch(() => {
        categoryPageButtonEnabled.value = true
        //viewCategoryFormRequestLoading.value = false;
        //openNotificationWithIcon('error',err.response.data.message);
      })
  }

  function storeCategory() {
    axios
      .post('/category/store', storeCategoryFormRequest, {
        headers: { Authorization: `Bearer ${$cookies.get(cookieData.accessTokenName)}` }
      })
      .then((res) => {
        openNotificationWithIcon('success', res.data.message, 5)
        listCategory()
        const haveSameContent = objectsHaveSameContentExcept(
          toRaw(viewCategoryFormRequest),
          viewCategoryFormRequestCache,
          'page'
        )
        if (haveSameContent) {
          categoryDataSourceMetaTotalCache.value++
        }
      })
      .catch((err) => {
        //categoryLoading.value = false;
        openNotificationWithIcon('error', err.response.data.message, 5)
      })
  }

  function updateCategory(id) {
    axios
      .post(`/category/update/${id}`, updateCategoryFormRequest, {
        headers: { Authorization: `Bearer ${$cookies.get(cookieData.accessTokenName)}` }
      })
      .then((res) => {
        openNotificationWithIcon('success', res.data.message, 5)
        listCategory()
      })
      .catch((err) => {
        categoryLoading.value = false
        openNotificationWithIcon('error', err.response.data.message, 5)
      })
  }

  function deleteCategory(id) {
    //categoryDataSource.value = []
    axios
      .get(`/category/delete/${id}`, {
        headers: { Authorization: `Bearer ${$cookies.get(cookieData.accessTokenName)}` }
      })
      .then((res) => {
        openNotificationWithIcon('success', res.data.message)
        listCategory()
        const haveSameContent = objectsHaveSameContentExcept(
          toRaw(viewCategoryFormRequest),
          viewCategoryFormRequestCache,
          'page'
        )
        if (haveSameContent) {
          categoryDataSourceMetaTotalCache.value--
        }
      })
      .catch((err) => {
        openNotificationWithIcon('error', err.response.data.message)
      })
  }

  function optionListCategory() {
    axios
      .get(`/category/option-list`, {
        headers: { Authorization: `Bearer ${$cookies.get(cookieData.accessTokenName)}` }
      })
      .then((res) => {
        //save the retrieved data to categoryDataSource state
        categoryOptionListSource.value = res.data.data
        categoryLoading.value = false
        //openNotificationWithIcon('success',res.data.message);
      })
      .catch((err) => {
        categoryLoading.value = false
        //openNotificationWithIcon('error',err.response.data.message);
      })
  }

  function restoreCategory() {}

  return {
    categoryLoading,
    categoryOptionListSource,
    categoryDataSource,
    categoryDataSourceMeta,
    viewCategoryFormRequest,
    storeCategoryFormRequest,
    updateCategoryFormRequest,
    deleteCategoryFormRequest,
    restoreCategoryFormRequest,
    viewCategoryFormRequestLoading,
    storeCategoryFormRequestLoading,
    updateCategoryFormRequestLoading,
    deleteCategoryFormRequestLoading,
    currentSortedColumn,
    categoryExportLoading,
    listCategory,
    optionListCategory,
    storeCategory,
    updateCategory,
    deleteCategory,
    restoreCategory,
    exportToFile
  }
})
