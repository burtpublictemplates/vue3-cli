import { ref, computed, reactive } from 'vue'
import { defineStore } from 'pinia'

export const useEnvStore = defineStore('envStore', () => {

 //data *************************************/   
  const sideBarMenu=reactive({
    collapsed:false,
    selectedKeys:ref(),
    openKeys:ref(),
    logoUrl:ref(import.meta.env.VITE_LOGO_TEXT),  
   
  })

  const count=ref(0)

  //getters *********************************/
  const doubleCount = computed(() => count.value++ )

  //methods ********************************/
  function increment() {
    count.value++
  }

  return { sideBarMenu, doubleCount, increment }
})
